<?php
namespace CicoWang;
//namespace Aliyun\DySDKLite;

//require_once dirname(__DIR__) . "/SignatureHelper.php";

use CicoWang\SignatureHelper;

class AliyunSms{
    // *** 需用户填写部分 ***
    // fixme 必填：是否启用https
    private $security = false;

    // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
    private $accessKeyId = "Your accessKeyId";
    private $accessKeySecret = "Your accessKeySecret";


    public function __construct($ak = '',$sk = '',$security = false){
        $this->accessKeyId = $ak;
        $this->accessKeySecret = $sk;
	}
    /**
     * 发送短信
     */
    function sendSms($params = []) {

        //$params = [];

        // fixme 必填: 短信接收号码
        //$params["PhoneNumbers"] = "17000000000";

        // fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
        //$params["SignName"] = "短信签名";

        // fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
        //$params["TemplateCode"] = "SMS_0000001";

        // fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
        //$params['TemplateParam'] = ["code" => "12345"];

        // fixme 可选: 设置发送短信流水号
        //$params['OutId'] = "12345";

        // fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
        //$params['SmsUpExtendCode'] = "1234567";


        // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
        if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
            $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
        }
        
        return $this->action('SendSms',$params);

    }
    function querySendDetails($params = []){
        //$params = [];

        // fixme 必填: 短信接收号码
        //$params["PhoneNumber"] = "17000000000";
    
        // fixme 必填: 短信发送日期，格式Ymd，支持近30天记录查询
        //$params["SendDate"] = "20170710";
    
        // fixme 必填: 分页大小
        //$params["PageSize"] = 10;
    
        // fixme 必填: 当前页码
        //$params["CurrentPage"] = 1;
    
        // fixme 可选: 设置发送短信流水号
        //$params["BizId"] = "yourBizId";
    
        // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
        return $this->action('QuerySendDetails',$params);
    }
    function action($action = '',$params = []){
        // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
        $helper = new SignatureHelper();

        // 此处可能会抛出异常，注意catch
        $content = $helper->request(
            $this->accessKeyId,
            $this->accessKeySecret,
            "dysmsapi.aliyuncs.com",
            array_merge($params, array(
                "RegionId" => "cn-hangzhou",
                "Action" => $action,
                "Version" => "2017-05-25",
            )),
            $this->security
        );

        return $content;
    }
}
?>

