<?php
require_once '../vendor/autoload.php';
 
use CicoWang\AliyunSms;

$ak = 'Your accessKeyId';
$sk = 'Your accessKeySecret';
$app = new AliyunSms($ak,$sk);
$params = [];

// fixme 必填: 短信接收号码
$params["PhoneNumbers"] = "17000000000";

// fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
$params["SignName"] = "短信签名";

// fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
$params["TemplateCode"] = "SMS_0000001";

// fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
$params['TemplateParam'] = ["code" => "12345"];

// fixme 可选: 设置发送短信流水号
$params['OutId'] = "12345";

// fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
$params['SmsUpExtendCode'] = "1234567";

var_dump($app->sendSms($params));
?>

